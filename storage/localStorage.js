import { AsyncStorage } from 'react-native';
import React  from 'react';

export const loadState = () => {
    try {
        const serializedState = localStorage.getItem('state');
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    } catch (err) {
        return undefined;
    }
};

export const saveState = (state) => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('state', serializedState);
    } catch {
        // ignore write errors
    }
};

// export const saveSettings = (settings) => {
//   AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(settings));
// }

// export const loadSettings = async () => {
//   try {
//     let settings = await AsyncStorage.getItem(STORAGE_KEY);

//     if (settings === null) { return DEFAULT_SETTINGS; }

//     return JSON.parse(settings);
//   } catch (error) {
//     console.log('Error loading settings', error);
//   }
// }




export const saveBoxId = async (boxId) => {
	try {

	  var boxes = await AsyncStorage.getItem('boxes');

	  if (boxes === null) {
		  boxes = '[]';
	  } 

      boxes = JSON.parse(boxes);

	  if (boxes.includes(boxId) === false) {
			boxes.push(boxId);
		  await AsyncStorage.setItem('boxes', JSON.stringify(boxes));
      }
      

	} catch (error) {
	  // Error retrieving data
	  console.log(error.message);
	}
  };

export const getBoxId = async () => {
  try {

	var boxes = await AsyncStorage.getItem('boxes');
	boxes = JSON.parse(boxes);
    if (boxes !== null) {
      // We have data!!
      return boxes; 
    } else {
       return false;
    }
    // return boxId;

  } catch (error) {
    // Error retrieving data
    console.log(error.message);
  }
}