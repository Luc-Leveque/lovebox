import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux'
import store from '../Store/configureStore'
import { saveBoxId, getBoxId } from '../storage/localStorage';
import { AsyncStorage } from 'react-native';




export class BoxList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			boxes: [],
			texte : 'Aucune Lovebox n\'est enregistrée. Ajoutez en !',
			value : getBoxId()
		};


	}

	componentDidMount() {

		console.log(store.getState().addedBox)

		store.subscribe(() => {
			this.setState({
				boxes: store.getState().addedBox
			});
		});
	}

	render() {

		if (store.getState().addedBox.length > 0) {
			return (
				<View styles={styles.boxListStyle}>
					{store.getState().addedBox.map(box => (
						<Text key={box} style={styles.boxStyle}>Lovebox {box}</Text>)
					)}
				</View>
			)
		} else {
			return (
				<View styles={styles.boxListStyle}>
					<Text style={styles.boxStyle}>{this.state.texte}</Text>
				</View>
			);
		}
	}
}

const styles = StyleSheet.create({
	boxListStyle: {
		paddingTop: 25,
	},
	boxStyle: {
		padding: 10,
		margin: 10,
		backgroundColor: 'rgba(207, 207, 207, .4)',
		borderRadius: 20,
		color: 'rgba(96,100,109, 1)'

	}
});

const mapStateToProps = (state) => {
	return {
		addedBox: state.addedBox
	}
}

export default connect(mapStateToProps)(BoxList)