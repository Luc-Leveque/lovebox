import React, { Component } from 'react';
import { StyleSheet, Text, View,  ScrollView, Keyboard, TouchableOpacity } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import { Dropdown } from 'react-native-material-dropdown';
import axios from 'axios';
import { connect } from 'react-redux'
import store from '../Store/configureStore'
import { showMessage, hideMessage } from "react-native-flash-message";
import { saveBoxId, getBoxId } from '../storage/localStorage';
import { AsyncStorage } from 'react-native';


export class MessageForm extends Component {
  constructor(props) {
    super(props);
  
    this.state = { 
        message: '',
        texte : 'Aucune Lovebox n\'est enregistrée. Pour envoyer un message, ajoutez en !',
        boxes: [],
        data: [],
    }

    console.log(store.getState())

  
    this._getStorageValue()

    this.handleBoxNumberChange = this.handleBoxNumberChange.bind(this);
    this.handleMessageChange = this.handleMessageChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  async _getStorageValue(){

    var value = await AsyncStorage.getItem('boxes')
    value = JSON.parse(value)
    if(value != null  ){
      value.map((box) => {
        if (!this.state.boxes.includes(box)) {
          const action = { type: "ADD_BOX", value: box };
          store.dispatch(action)
        }
      });
    }
  }


  handleBoxNumberChange(boxNumber) {
    this.setState({ boxNumber });
  }

  handleMessageChange(message) {
    this.setState({ message });
  }

  handleSubmit() {
    // saveSettings(this.state);

    axios.post('https://lovebox2019.herokuapp.com/api/v1/messages', {
        message: this.state.message,    
        box_id: this.state.boxNumber,
      })
      .catch(function (error) {
      });


      showMessage({
        message: "Le message a bien été envoyé.",
        type: "success",
      });

      this.setState({message : ''});
  }

  componentDidMount() {

    store.subscribe(() => {
      const lastBox = store.getState().addedBox.slice(-1).pop();
      const obj = {'value':lastBox}
      this.setState({
        data: [...this.state.data, obj]
      });
    });
  }

  render() {

      if (store.getState().addedBox.length > 0) {
        return (
          <View style={styles.container}>
          <ScrollView>
            <View style={styles.inputContainer}>
            <Dropdown
                label="Numéro de la Lovebox"
                baseColor="#ffab91"
                data={this.state.data}
                value={this.state.boxNumber}
                onChangeText={this.handleBoxNumberChange}
                Required
            />
            <TextField
                tintColor='#cfd8dc'
                baseColor="#ffab91"
                label="Message"
                onBlur={Keyboard.dismiss}
                value={this.state.message}
                onChangeText={this.handleMessageChange}
                multiline = {true}
            />
            </View>
            <View style={styles.inputContainer}>
              <TouchableOpacity
                style={styles.saveButton}
                onPress={this.handleSubmit}
              >
                <Text style={styles.saveButtonText}>Envoyer</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
      </View>
        )
      } else {
        return (
          <View styles={styles.boxListStyle}>
					  <Text style={styles.boxStyle}>{this.state.texte}</Text>
				  </View>
        )
      };
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  inputContainer: {
  },

  textArea: {
    height: 100,
    borderColor: '#CCCCCC',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    paddingLeft: 20,
    paddingRight: 20
  },

  saveButton: {
    borderWidth: 1,
    borderColor: '#9b0000',
    backgroundColor: '#9b0000',
    padding: 15,
    margin: 5,
    borderRadius: 4
  },
  saveButtonText: {
    color: '#FFFFFF',
    fontSize: 20,
    textAlign: 'center'
  },

  boxListStyle: {
		paddingTop: 25,
	},
	boxStyle: {
		padding: 10,
		margin: 10,
		backgroundColor: 'rgba(207, 207, 207, .4)',
		borderRadius: 20,
    color: 'rgba(96,100,109, 1)'
  }


});

const mapStateToProps = (state) => {
	return {
		addedBox: state.addedBox
	}
}

export default connect(mapStateToProps)(MessageForm)