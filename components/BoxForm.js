import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Keyboard, TouchableOpacity } from 'react-native';
import { saveBoxId } from '../storage/localStorage';
import { TextField } from 'react-native-material-textfield';
import FlashMessage from "react-native-flash-message";
import { showMessage, hideMessage } from "react-native-flash-message";
import axios from 'axios';
import { connect } from 'react-redux';
import store from '../Store/configureStore'


class BoxForm extends Component {
	constructor(props) {

		super(props);

		this.state = { number: '' }

		this.handleNumberChange = this.handleNumberChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	async handleSubmit() {

		let boxId = {
			number: this.state.number
		}

		let etat = '0';
		axios.get(`https://lovebox2019.herokuapp.com/api/v1/boxes`)
			.then(res => {
				const boxes = res.data;
				boxes.map((box) => {

					if (box.id == boxId.number) {
						if (!store.getState().addedBox.includes(boxId.number)) {
							const action = { type: "ADD_BOX", value: this.state.number };
							this.props.dispatch(action)

							saveBoxId(this.state.number);

							showMessage({
								message: "La Lovebox a été ajouté.",
								type: "success",
							});

							etat = '1';
						} else {
							etat = '2';
						}
					}
				});

				if (etat == 0) {
					showMessage({
						message: "La Lovebox n'existe pas.",
						type: "danger",
					});
				} else if (etat == 2) {
					showMessage({
						message: "La Lovebox est déjà enregistrée.",
						type: "danger",
					})
				}
			})
	}

	handleNumberChange(num) {
		this.setState({ number: num });
	}

	componentDidMount() {
	}

	render() {
		return (
			<View style={styles.container}>
				<ScrollView>
					<View style={styles.inputContainer}>
						<TextField
							tintColor='#cfd8dc'
							baseColor="#ffab91"
							label="Numéro de la lovebox"
							onBlur={Keyboard.dismiss}
							value={this.state.number}
							onChangeText={this.handleNumberChange}
							multiline={true}
						/>
					</View>
					<View style={styles.inputContainer}>
						<TouchableOpacity
							style={styles.saveButton}
							onPress={() => this.handleSubmit()}
						>
							<Text style={styles.saveButtonText}>Enregistrer</Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
				<FlashMessage position="top"/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	header: {
		fontSize: 25,
		textAlign: 'center',
		margin: 10,
		fontWeight: 'bold'
	},

	inputContainer: {
		
	},
	textInput: {
		borderColor: '#CCCCCC',
		borderTopWidth: 1,
		borderBottomWidth: 1,
		height: 50,
		fontSize: 25,
		paddingLeft: 20,
		paddingRight: 20
	},

	saveButton: {
		borderWidth: 1,
		borderColor: '#9b0000',
		backgroundColor: '#9b0000',
		padding: 15,
		margin: 5,
		borderRadius: 4
	},
	saveButtonText: {
		color: '#FFFFFF',
		fontSize: 20,
		textAlign: 'center'
	}
});

const mapStateToProps = (state) => {
	return {
		number: state.number
	}
}

export default connect(mapStateToProps)(BoxForm)