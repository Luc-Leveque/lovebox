import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import BoxForm from '../components/BoxForm';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


export default class BoxFormScreen extends Component {

  componentDidMount() {
  }
  
  render() {
    return (
      <View style={styles.container}>
        <Icon name="arrow-left" style={styles.arrow} size={30} onPress={() => { this.props.navigation.goBack() }} />
        <View style={styles.titleItem}>
            <Icon name="heart-box-outline" style={styles.heart} size={80} />
            <Text style={styles.text}>Lovebox</Text>
            <Icon name="circle-medium" style={styles.circle} size={20} />

            <Text style={styles.text}>
              Ajouter une Lovebox
            </Text>
        </View>
            
        <View style={styles.form}>
          <BoxForm/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 45,
    backgroundColor: 'white',
  },
  titleItem: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  arrow: {
    padding : 5,
    color: '#9b0000'
  },
  heart: {
    color: '#9b0000'
  },
  circle: {
    color: '#cfcfcf'
  },
  text: {
    padding: 15,
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  form: {
    paddingLeft: 50,
    paddingRight: 50,
    flex: 1,
  }
});

