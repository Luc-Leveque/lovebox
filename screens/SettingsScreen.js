import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { BoxList } from '../components/BoxList';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import { getBoxId } from '../storage/localStorage';

export default class SettingsScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  async componentDidMount(){
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.titleItem}>
            <Icon name="heart-box" style={styles.heart} size={80} />
            <Text style={styles.text}>Lovebox</Text>
            <Icon name="circle-medium" style={styles.circle} size={20} />

            <Text style={styles.text}>
              Vos Loveboxes
            </Text>
        </View>
            
        <View style={styles.boxList}>
          <BoxList />
          <TouchableOpacity style={styles.add}>
            <Icon  name="plus-circle" size={70} color="#900" onPress={() => this.props.navigation.navigate('BoxForm')} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 60,
    backgroundColor: 'white',
  },
  titleItem: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  arrow: {
    padding : 5,
    color: '#9b0000'
  },
  heart: {
    color: '#9b0000'
  },
  circle: {
    color: '#cfcfcf'
  },
  text: {
    padding: 15,
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  boxList: {
    flex: 1,
    alignItems: 'center',
  },
  add: {
    position: 'absolute',                                          
    bottom: 20,                                                    
    right: 20,
  }
});

