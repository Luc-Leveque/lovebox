
import MainTabNavigator from './MainTabNavigator';
import BoxFormScreen from '../screens/BoxFormScreen';


import { createStackNavigator, createAppContainer } from "react-navigation";

const AppNavigator = createStackNavigator(
 {
   Main: MainTabNavigator,
   BoxForm: { screen: BoxFormScreen }
 },
 {
   initialRouteName: "Main",
   headerMode: 'none'
 }
);

export default createAppContainer(AppNavigator);