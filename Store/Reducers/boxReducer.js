const initialState = { addedBox: [] }


function addBox(state = initialState, action) {
  switch (action.type) {
    case 'ADD_BOX':
        const addedBoxIndex = state.addedBox.findIndex(item => item.id === action.value)
        if (addedBoxIndex !== -1) {
            return state;
        } else {
            return { 
                ...state,
                addedBox: [...state.addedBox, action.value]
            }
        }
    default:
        return state;
    }
}

export default addBox;