// Store/configureStore.js

import { createStore } from 'redux';
import addBox from './Reducers/boxReducer'
import { loadState, saveState } from '../storage/localStorage';

const persistedState = loadState();
const store = createStore(addBox, persistedState);

store.subscribe(() => {
    saveState({
        boxes: store.getState().addedBox
    });
  });

export default store;
